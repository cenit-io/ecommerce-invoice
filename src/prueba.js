import React from 'react'
import {Card} from '@shopify/polaris';

class Prueba extends React.Component{
    render(){
        return(
            <Card title={this.props.title} sectioned>
                <p>View a summary of your online store’s performance.</p>
            </Card>
        );
    }
}

export default Prueba;