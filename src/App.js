import React from 'react';
import './App.css';
import '@shopify/polaris/styles.css'
import {Route, Switch, withRouter} from 'react-router-dom';
import {
    AppProvider, Navigation, Icon,
    Frame, ContextualSaveBar, Card, Toast, TopBar, Page,
    Layout, FormLayout, TextField, SkeletonPage, TextContainer,
    ActionList, Loading, SkeletonDisplayText, Modal, SkeletonBodyText
} from '@shopify/polaris';

import Home from "./main/Home";
import Company from "./main/Company";
// import MarketPlaceConfig from "./marketPlace/MarketPlaceConfig";
import Templates from "./templates/Templates";
import Configuration from "./configurations/Configuration";
import InvoiceContainer from "./templates/invoice/InvoiceContainer";
import Orders from "./orders/Orders";
import Subscription from "./main/Subscription";
// import PlatFormsContainer from "./marketPlace/PlatFormsContainer";
import PlatFormsOptions from "./marketPlace/PlatFormsOptions";
import PlatFormsContainer from "./marketPlace/PlatFormsContainer";


 class App extends React.Component {
    defaultState = {
        emailFieldValue: 'dharma@jadedpixel.com',
        nameFieldValue: 'Jaded Pixel',
    };

    state = {
        showToast: false,
        isLoading: false,
        isDirty: false,
        searchActive: false,
        searchText: '',
        userMenuOpen: false,
        showMobileNavigation: false,
        modalActive: false,
        nameFieldValue: this.defaultState.nameFieldValue,
        emailFieldValue: this.defaultState.emailFieldValue,
        storeName: this.defaultState.nameFieldValue,
        supportSubject: '',
        supportMessage: '',
    };

    handleNavClick=(key)=>{
        return (ev) => {
            console.log(ev);
            this.props.history.push(key);
        }
    };

    render() {
        const {
            showToast,
            isLoading,
            isDirty,
            searchActive,
            searchText,
            userMenuOpen,
            showMobileNavigation,
            nameFieldValue,
            emailFieldValue,
            modalActive,
            storeName,
        } = this.state;

        const userMenuActions = [
            {
                items: [{content: 'Community forums'}],
            },
        ];

        const topBarMarkup = (
            <TopBar
                showNavigationToggle={true}
                onNavigationToggle={this.toggleState('showMobileNavigation')}
            />
        );


        const loadingMarkup = isLoading ? <Loading/> : null;

        const actualPageMarkup = (
            <Page title={'ECOMMERCE APP'}>
                <Switch>
                    <Route exact path={'/'} component={Home}/>
                    {/*<Route path={'/orders'} component={Orders1}/>*/}
                    <Route path={'/templates'} component={Templates}/>
                    <Route path={'/company'} component={Company}/>
                    {/*<Route path={'/marketplace'} component={MarketPlaceConfig}/>*/}
                    {/*<Route path={'/marketplace'} component={MarketOptions}/>*/}
                    <Route path={'/platforms'} component={PlatFormsOptions}/>
                    <Route path={'/spree'} component={PlatFormsContainer}/>
                    <Route path={'/woo'} component={PlatFormsContainer}/>
                    <Route path={'/mag'} component={PlatFormsContainer}/>
                    <Route path={'/configuration'} component ={Configuration}/>
                    <Route path={'/invoice'} component ={InvoiceContainer}/>
                    <Route path={'/orders'} component={Orders}/>
                    <Route path={'/subscription'} component={Subscription}/>
                </Switch>
            </Page>
        );

        const loadingPageMarkup = (
            <SkeletonPage>
                <Layout>
                    <Layout.Section>
                        <Card sectioned>
                            <TextContainer>
                                <SkeletonDisplayText size="small"/>
                                <SkeletonBodyText lines={9}/>
                            </TextContainer>
                        </Card>
                    </Layout.Section>
                </Layout>
            </SkeletonPage>
        );

        const pageMarkup = isLoading ? loadingPageMarkup : actualPageMarkup;

        const modalMarkup = (
            <Modal
                open={modalActive}
                onClose={this.toggleState('modalActive')}
                title="Contact support"
                primaryAction={{
                    content: 'Send',
                    onAction: this.toggleState('modalActive'),
                }}
            >
                <Modal.Section>
                    <FormLayout>
                        <TextField
                            label="Subject"
                            value={this.state.supportSubject}
                            onChange={this.handleSubjectChange}
                        />
                        <TextField
                            label="Message"
                            value={this.state.supportMessage}
                            onChange={this.handleMessageChange}
                            multiline
                        />
                    </FormLayout>
                </Modal.Section>
            </Modal>
        );

        const navigationMenu = (
            <div style={{height: '600px'}}>
                <Navigation location='/'>
                    <Navigation.Section title={'MENU'} items={[{ label: 'Home', icon: 'home',onClick: this.handleNavClick('/')},
                        {label: 'Company Info', icon: 'profile',onClick: this.handleNavClick('/company')},
                        {label: 'Ecommerce Source', icon: 'onlineStore', onClick: this.handleNavClick('/platforms')},
                        {label: 'Configurations', icon: 'circlePlus',onClick: this.handleNavClick('/configuration')},
                        {label: 'Documents Settings', icon: 'notes', onClick: this.handleNavClick('/templates')},
                        { label: 'Orders', icon: 'orders', onClick: this.handleNavClick('/orders')}]}
                                        action={{
                                            icon: 'help',
                                            accessibilityLabel: 'Contact support',
                                            onClick: this.viewModal('modalActive'),
                                        }}/>
                    <Navigation.Section separator items={[{label: 'Subscription',icon: 'dispute', onClick: this.handleNavClick('/subscription')}]}/>
                </Navigation>
            </div>
        );

        const contextualSaveBarMarkup = isDirty ? (
            <ContextualSaveBar
                message="Unsaved changes"
                saveAction={{
                    onAction: this.handleSave,
                }}
                discardAction={{
                    onAction: this.handleDiscard,
                }}
            />
        ) : null;


        const toastMarkup = showToast ? (
            <Toast
                onDismiss={this.toggleState('showToast')}
                content="Changes saved"
            />
        ) : null;

        const theme = {
            colors: {
                topBar: {
                    background: "rgba(52,7,51,0.53)",//'#357997',
                },
            },
        };


        return (
            <AppProvider theme={theme}>
                <Frame topBar={topBarMarkup} navigation={navigationMenu} showMobileNavigation={showMobileNavigation}
                       onNavigationDismiss={this.toggleState('showMobileNavigation')}>
                    {contextualSaveBarMarkup}
                    {loadingMarkup}
                    {pageMarkup}
                    {toastMarkup}
                    {modalMarkup}
                </Frame>
            </AppProvider>
        );

    }

    toggleState = (key) => {
        return () => {
            this.setState((prevState) => ({[key]: !prevState[key]}));
        };
    };

    handleSearchFieldChange = (value) => {
        this.setState({searchText: value});
        if (value.length > 0) {
            this.setState({searchActive: true});
        } else {
            this.setState({searchActive: false});
        }
    };

    /*handleSearchResultsDismiss = () => {
        this.setState(() => {
            return {
                searchActive: false,
                searchText: '',
            };
        });
    };*/

    handleEmailFieldChange = (emailFieldValue) => {
        this.setState({emailFieldValue});
        if (emailFieldValue != '') {
            this.setState({isDirty: true});
        }
    };

    handleNameFieldChange = (nameFieldValue) => {
        this.setState({nameFieldValue});
        if (nameFieldValue != '') {
            this.setState({isDirty: true});
        }
    };

    viewModal=(key)=>{
        return () => {
            this.setState((prevState) => ({[key]: !prevState[key]}));
        };
    };

    handleSave = () => {
        this.defaultState.nameFieldValue = this.state.nameFieldValue;
        this.defaultState.emailFieldValue = this.state.emailFieldValue;
        this.setState({
            isDirty: false,
            showToast: true,
            storeName: this.defaultState.nameFieldValue,
        });
    };

    handleDiscard = () => {
        this.setState({
            emailFieldValue: this.defaultState.emailFieldValue,
            nameFieldValue: this.defaultState.nameFieldValue,
            isDirty: false,
        });
    };

    handleSubjectChange = (supportSubject) => {
        this.setState({supportSubject});
    };

    handleMessageChange = (supportMessage) => {
        this.setState({supportMessage});
    };
} export default withRouter(App)

