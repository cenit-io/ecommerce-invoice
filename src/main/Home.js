import React, {Fragment} from 'react';
import {Card, FormLayout, Banner, Link,Icon} from '@shopify/polaris';

export default class Home extends React.Component{
    toggleState = (key) => {
        return (ev) => {
            console.log(ev)
            this.props.history.push(key);
        };
    };

    render() {
        return(
            <Fragment>
                <Card sectioned title={'HOME'}>
                    <FormLayout.Group>
                        <Banner icon="profile" title={<Link onClick={this.toggleState('/company')}>COMPANY INFO</Link>}>
                            <p>Information about the company</p>
                        </Banner>
                        <Banner icon="notes" title={<Link onClick={this.toggleState('/templates')}>TEMPLATES</Link>}>
                            <p>Templates Listing</p>
                        </Banner>
                    </FormLayout.Group>
                    <FormLayout.Group>
                        <Banner icon="onlineStore" title={<Link onClick={this.toggleState('/marketplace')}>MARKET PLACE</Link>}>
                            <p>Markets Listing</p>
                        </Banner>
                        <Banner icon="orders" title={<Link onClick={this.toggleState('/orders')}>ORDERS</Link>}>
                            <p>Orders Listing</p>
                        </Banner>
                    </FormLayout.Group>
                </Card>
            </Fragment>

        );
    }
}







