import React from 'react';
import {Card, FormLayout, TextField} from '@shopify/polaris';
import countryList from 'react-select-country-list';
import ImageDropZone from '../utils/ImageDropZone';
import SelectCombo from '../utils/SelectCombo';
import axios from "axios";

class Company extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            sending: false,
            image: [],
            country:'Afghanistan',
            companyName:'',
            email:'',
            city:'',
            providence:'',
            street:'',
            postalCode:'',
            number:'',
            logo: '',
            stamp: ''
        };
    }

    countryChange=(key)=>{
        return (value) => {
            const state = {};
            state[key] = countryList().getLabel(value);
            this.setState(state);
        }
    }

    handleChange(key){
        return (value) => {
            const state = {};
            state[key] = value;
            this.setState(state);
        }
    };

    handleLogoImageChange=(type,new_image)=>{
        if (type === 'Logo'){
            this.setState({logo:new_image, })
        }else {
            if (type === 'Stamp') {
                this.setState({ stamp:new_image, })
            }
        }
    }

    saveInCenit=()=>{
        console.log(this.state.logo)
        //solicitud get con axios.
        axios.post('https://cenit.io/app/cautionem-dev/company_info', this.state)
            .then(response=>{
                console.log(response);})
            .catch(e =>{
                // Mostrar los errores en la consola
                console.log(e);
            });
    };


    readAtributes(appContext){
        return(
            <div>
                <FormLayout>
                    <FormLayout.Group>
                        <FormLayout.Group>
                            <TextField label="Name" value={this.state.companyName} onChange={this.handleChange('companyName')} />
                            <SelectCombo title={"Country"} value={this.state.country} options={countryList().getData()} onChange={this.countryChange('country')}/>
                        </FormLayout.Group>
                        <FormLayout.Group>
                            <TextField label="Email" value={this.state.email} onChange={this.handleChange('email')} />
                            <TextField label="City" value={this.state.city} onChange={this.handleChange('city')} />
                        </FormLayout.Group>
                    </FormLayout.Group>
                    <FormLayout.Group>
                        <FormLayout.Group>
                            <TextField label="State/Providence/Region" value={this.state.providence} onChange={this.handleChange('providence')} />
                            <TextField label="Street 1" helpText={'Street address, P.O box'} value={this.state.street} onChange={this.handleChange('street')}/>
                        </FormLayout.Group>
                        <FormLayout.Group>
                            <TextField label="Zip/Postal Code" value={this.state.postalCode} onChange={this.handleChange('postalCode')} />
                            <TextField label="Number" value={this.state.number} helpText={'Apartment, suite, unit, building, floor, etc'} onChange={this.handleChange('number')}/>
                        </FormLayout.Group>
                    </FormLayout.Group>
                    <FormLayout.Group>
                        <FormLayout.Group>
                            <div style={{transform: 'translateX(50px)'}}><ImageDropZone title={'Logo'} onChange ={this.handleLogoImageChange}/></div>
                        </FormLayout.Group>
                        <FormLayout.Group>
                            <div style={{transform: 'translateX(50px)'}}><ImageDropZone title={'Stamp'} onChange ={this.handleLogoImageChange}/></div>
                        </FormLayout.Group>
                    </FormLayout.Group>
                </FormLayout>
            </div>
        )
    }

    render() {
        return(
            <div>
                <Card title={'COMPANY INFO'} primaryFooterAction={{ content: 'Save', onAction: this.saveInCenit }}>
                    <Card.Section>
                        {this.readAtributes()}
                    </Card.Section>

                </Card>
            </div>
        );
    }


}export default Company;