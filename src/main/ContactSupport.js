import React from 'react';
import {Modal,FormLayout,TextField} from '@shopify/polaris';

class ContactSupport extends React.Component{

    constructor(props){
        super(props);
        const{onClick}=this.props;

    }

    render() {
        return(
            <Modal title='Contact support' onClick={'modalActive'}   primaryAction={{content: 'Send'}}>
                    <TextField label='Subject'/>
                    <TextField label='Message' multiline/>
            </Modal>
        );
    }


}export default ContactSupport;