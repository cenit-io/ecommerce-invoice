import React from 'react';
import { HashRouter } from "react-router-dom"
import App from "../App";


class Container extends React.Component{
    render(){
        return(
            <HashRouter>
                <App/>
            </HashRouter>
        );
    }
}

export default Container;