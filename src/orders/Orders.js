import React from 'react';
import {Card,FilterType,ResourceList,TextStyle,Button} from '@shopify/polaris'

class Orders extends React.Component{
    state = {
        selectedItems: [],
        sortValue: 'DATE_MODIFIED_DESC',
        searchValue: '',
        appliedFilters: []
    };

    itemsList = [
        {
            id: 341,
            url: 'order/341',
            name: 'Primera orden',
            location: 'Decatur, USA',
            latestOrderUrl: 'orders/1456',
        },
        {
            id: 256,
            url: 'order/256',
            name: 'Segunda orden',
            location: 'Los Angeles, USA',
            latestOrderUrl: 'orders/1457',
        },
    ];

    filters = [
        {
            key: 'orderChannel',
            label: 'Channel of orders',
            type: FilterType.TextField,
        },
        {
            key: 'orderid',
            label: 'Number of orders',
            type: FilterType.Select,
        },
    ];

    resourceName = {
        singular: 'order',
        plural: 'orders',
    };

    sortOptions=[
        {label: 'Channel', value: 'DATE_MODIFIED_DESC'},
        {label: 'Oldest update', value: 'DATE_MODIFIED_ASC'}];

    handleSearchChange = (searchValue) => {
        this.setState({searchValue});
    };
    handleFiltersChange = (appliedFilters) => {
        this.setState({appliedFilters});
    };
    handleSelectionChange = (selectedItems) => {
        console.info("items", selectedItems)
        this.setState({selectedItems});
    };

    handleSortChange=(selected)=>{
        this.setState({sortValue: selected});
        console.log(`Sort option changed to ${selected}.`);
    }

    renderItem = (item) => {
        const {id, url, name, location, latestOrderUrl} = item;
        const shortcutActions = [{content: 'Show Invoice'},{content: 'Resend Invoice'}];
        return (
            <ResourceList.Item id={id} url={url} shortcutActions={shortcutActions} persistActions>
                <TextStyle variation="strong">{name}</TextStyle>
                <div>{location}</div>
            </ResourceList.Item>
        );
    };

    bulkActions = [
        {
            content: 'Resend Invoice',
            onAction: () => console.log('Todo: implement bulk add tags'),
        }
    ];



    render() {

        const filterControl = (
            <ResourceList.FilterControl filters={this.filters} appliedFilters={this.state.appliedFilters}
                onFiltersChange={this.handleFiltersChange}searchValue={this.state.searchValue} onSearchChange={this.handleSearchChange}/>
        );

        return(
            <Card title={'ORDERS'}>
                <ResourceList resourceName={this.resourceName} items={this.itemsList} renderItem={this.renderItem}
                              selectedItems={this.state.selectedItems} onSelectionChange={this.handleSelectionChange}
                              sortValue={this.state.sortValue} filterControl={filterControl} sortOptions={this.sortOptions}
                              onSortChange={this.handleSortChange} bulkActions={this.bulkActions}/>
            </Card>
        );
    }

}export default Orders;