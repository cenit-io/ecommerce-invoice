import React from 'react';
import {Page} from "@shopify/polaris";
import {Route, Switch} from 'react-router-dom';
import MarketPlaceConfig from "./MarketPlaceConfig";
import WoocommerceConfig from "./WoocommerceConfig";
import EcommerceConfig from "./EcommerceConfig";

class PlatFormsContainer extends React.Component{

    render() {
        return(
            <Page>
                <Switch>
                    <Route exact path={'/spree'} component={MarketPlaceConfig}/>
                    <Route path={'/woo'} component={WoocommerceConfig}/>
                    <Route path={'/mag'} component={EcommerceConfig}/>
                </Switch>
            </Page>
        );
    }

}export default PlatFormsContainer;