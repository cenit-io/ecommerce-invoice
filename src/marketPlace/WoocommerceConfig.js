import React from 'react';
import {Card, FormLayout, TextField,DisplayText,Checkbox,Select} from '@shopify/polaris';
import axios from 'axios';

class WoocommerceConfig extends React.Component{

    state = {
        selected: 'selectione',
        sendChecked: false,
        receiveChecked: true,
        actionSelection:'',
        storeName:'Insert the name of store',
        storeUrl: 'Insert the url of store',
        storeToken:''

    };

    selectChange = (newValue) => {
        this.setState({selected: newValue});
    };

    sendChange = (newValue)=>{
        this.setState({actionSelection : newValue});
        this.setState({sendChecked: true});
        this.setState({receiveChecked: false});
    };

    receiveChange = (newValue)=>{
        this.setState({actionSelection : newValue});
        this.setState({sendChecked: false});
        this.setState({receiveChecked: true});
    };

    handleChange(key){
        return (value) => {
            const state = {};
            state[key] = value;
            this.setState(state);
        }
    };

    saveInCenit=()=>{
        //solicitud get con axios.
        // axios.post('https://cenit.io/app/cautionem-dev/testing', this.state)
        //     .then(response=>{
        //         console.log(response);})
        //     .catch(e =>{
        //         // Mostrar los errores en la consola
        //         console.log(e);
        //     });
    };


    render() {
        const options=[
            {label: 'Selectione', value:'selectione'},
            {label: 'Orders', value:'orders'},
            {label: 'Shipments', value: 'shipments'}];

        return(
            <Card title={'WOOCOMMERCE CONFIGURATION'} primaryFooterAction={{content: 'save', onAction: this.saveInCenit}}>
                <Card.Section>
                    <FormLayout.Group>
                        <TextField label="Store Name" value={this.state.storeName} onChange={this.handleChange('storeName')} />
                        <TextField label="Store URL" value={this.state.storeUrl} onChange={this.handleChange('storeUrl')}/>
                    </FormLayout.Group>
                    <FormLayout.Group>
                        <TextField label="Token" value={this.state.storeToken} onChange={this.handleChange('storeToken')} />
                    </FormLayout.Group>
                </Card.Section>
                <Card.Section title={'Resources'} >
                    <FormLayout.Group>
                        <DisplayText size={'small'}> I want to</DisplayText>
                        <Checkbox label={'Receive'} checked={this.state.receiveChecked} onChange={this.receiveChange}/>
                        <Checkbox label={'Send'} checked={this.state.sendChecked} onChange={this.sendChange}/>
                        <Select options={options} onChange={this.selectChange} value={this.state.selected} />
                    </FormLayout.Group>
                </Card.Section>
            </Card>
        );
    }
}export default WoocommerceConfig;