import React, {Fragment} from 'react';
import {Card, Banner, Link} from '@shopify/polaris';

class PlatFormsOptions extends React.Component{

    toggleState = (key) => {
        return (ev) => {
            console.log(ev)
            this.props.history.push(key);
        };
    };


    render (){
        return(
            <Card sectioned title={'PLATFORMS'}>
                <Banner icon="circleChevronRight" title={<Link onClick={this.toggleState('/spree')}>SPREE</Link>}/>
                <Banner icon="circleChevronRight" title={<Link onClick={this.toggleState('/mag')}>MAGENTO</Link>}/>
                <Banner icon="circleChevronRight" title={<Link onClick={this.toggleState('/woo')}>WOOCOMMERCE</Link>}/>
            </Card>
        );
    }

}export default PlatFormsOptions;