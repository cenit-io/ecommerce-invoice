import React from 'react';
import {ChoiceList} from '@shopify/polaris';

class SelectionCheckBox extends React.Component {

    constructor(props){
        super(props);
        this.state ={
            selected: this.props.selected,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value){
        this.setState({
            selected: value
        })
    };

    render(){
        const {selected} = this.state;
        const {options, title} = this.props;

        return (
            <ChoiceList
                title={title}
                choices={[
                    {label: options[0], value: options[0]},
                    {label: options[1], value: options[1]}
                ]}
                selected={selected}
                onChange={this.handleChange}
            />
        );
    }
}

export default SelectionCheckBox;