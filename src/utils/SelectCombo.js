import React from 'react';
import {Select} from '@shopify/polaris';

class SelectCombo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: 'today',
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(newValue){
        this.setState({selected: newValue});
        this.props.onChange(newValue)
    };

    render() {
        const{title} = this.props;

        return (
            <Select
                label={title}
                options={this.props.options}
                onChange={this.handleChange}
                value={this.state.selected}
                style={{width: 30}}
            />
        );
    }
}
export default SelectCombo;