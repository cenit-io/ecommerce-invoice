import React from 'react';
import {Checkbox} from '@shopify/polaris';

class CheckBoxElement extends React.Component {

    handleChange=()=>{
        this.props.update(this.props.tab_name,this.props.id,1, !this.props.data[1])

    }

    render() {
        const{label, checked} = this.props;

        return (
            <Checkbox
                checked={checked}
                label={label}
                onChange={this.handleChange}
                disabled={!checked}
            />
        );
    }
}

export default CheckBoxElement;