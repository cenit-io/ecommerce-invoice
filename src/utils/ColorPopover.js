import React, {Fragment} from 'react';

import {Popover, FormLayout, Button, ColorPicker, Card, TextField, Stack} from '@shopify/polaris';
import {hsbToHex, rgbToHsb} from '@shopify/polaris';


class ColorPopover extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            active: false,
            tagValue: '',
            color: this.props.color,
            color_text: this.props.color,
        }

        this.togglePopover = this.togglePopover.bind(this);
        this.handleChange = this.handleChange.bind(this);

        this.showPopover = this.showPopover.bind(this);

        this.hexToRgb = this.hexToRgb.bind(this);
        this.formatHexToHsb = this.formatHexToHsb.bind(this);
    }

    togglePopover(){
        this.setState(({active}) => {
            return {active: !active};
        });
    };

    showPopover(){
        this.setState(({active}) => {
            return {active: !active};
        });
    }

    formatHexToHsb(hex){
        return rgbToHsb(this.hexToRgb(hex));
    }

    hexToRgb(hex) {
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            red: parseInt(result[1], 16),
            green: parseInt(result[2], 16),
            blue: parseInt(result[3], 16)
        } : null;
    }

    handleChange(color){
        let hexColor = hsbToHex(color)

        this.setState({
            color: hexColor,
            color_text: hexColor
        });

        this.props.onChange(hexColor);
    };


    render(){
        const {active, color} = this.state;
        const {text_label} = this.props;


        const activator = (
           <Stack wrap={false} alignment="leading" spacing="tight">

                   <Stack spacing="tight">
                        <TextField label={text_label} value={color} disabled={true} labelAction={{content: active? 'ok':'Change', onAction:this.showPopover}} />
                        <div style={{width:20, height: 20, backgroundColor: `${color}`, transform:'translateY(30px)'}}> </div>
                    </Stack>

            </Stack>

        );

        return (
            <Popover
                active={active}
                activator={activator}
                onClose={this.togglePopover}
                sectioned
            >
                    <ColorPicker color={this.formatHexToHsb(color)} onChange={this.handleChange}/>
            </Popover>
        );
    }
}

export default ColorPopover;