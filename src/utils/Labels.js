import React from 'react';

import {Stack, Checkbox, TextField} from '@shopify/polaris';


class Labels extends React.Component{

    handleLabelCheck=()=>{
        this.props.update(this.props.tab_name,this.props.id,0, !this.props.data[0])

    }
    handleSubLabelCheck = () => {
        this.props.update(this.props.tab_name,this.props.id,1, !this.props.data[1])
    };

    handleSubLabelChange = (value) => {
        this.props.update(this.props.tab_name,this.props.id,3, value)
    };

    handleLabelChange=(value)=>{
        this.props.update(this.props.tab_name,this.props.id,2, value)
    }





    render(){
        const [label, sub_label,text_label,text_sub] = this.props.data;

        return(
            <div className='labels-wrapper'>
                <Stack wrap={false}>
                    <Checkbox
                        checked={label}
                        onChange={this.handleLabelCheck}
                        disabled={!label}
                    />
                    <TextField id={this.props.id} value={text_label} disabled={!label} onChange={this.handleLabelChange}/>

                    <Checkbox
                        checked={sub_label}
                        onChange={this.handleSubLabelCheck}
                        disabled={!sub_label}
                    />
                    <TextField id={this.props.id} value={text_sub} disabled={!sub_label} onChange={this.handleSubLabelChange}/>
                </Stack>
            </div>
        );
    }

}
export default Labels;