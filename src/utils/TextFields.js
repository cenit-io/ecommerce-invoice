import React from 'react';
import {TextField} from '@shopify/polaris';

class  TextFields extends React.Component{

    handleChange=(value)=>{
        this.props.update(this.props.tab_name,this.props.id,2, value)
    }

    render() {
        return(
            <TextField id={this.props.id} value={this.props.value} disabled={this.props.disabled} onChange={this.handleChange} />
        );
    }

}export default TextFields;