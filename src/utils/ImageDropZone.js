import React, {Fragment} from 'react';
import {Stack, Thumbnail, DropZone, Card} from '@shopify/polaris';
import axios from 'axios';


class ImageDropZone extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            images: [],
        };
    }

    handleDeleteImage() {
        this.setState({ images: [] })
    }

    render() {
        const {images} = this.state;
        const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];

        const fileUpload = !images.length && <DropZone.FileUpload />;
        const uploadedFiles = images.length > 0 && (
            <Stack vertical>
                {images.map((image, index) => (
                    <Stack alignment="center" key={index}>
                        <Thumbnail
                            size="large"
                            alt={image.name}
                            source={
                                validImageTypes.indexOf(image.type) > 0
                                    ? window.URL.createObjectURL(image) :'https://cdn.shopify.com/s/files/1/0757/9955/files/New_Post.png?12678548500147524304'
                            }
                        />
                        <div>
                            {image.name}
                        </div>
                    </Stack>
                ))}
            </Stack>
        );

        if(images.length != 0){
            return (<Fragment>
                    <div style={{width: 150, height: 140}}>
                    <Card title={this.props.title} actions={[{content:"Delete", onAction: this.handleDeleteImage}]} >
                        <img style={{display: 'block', marginLeft: 'auto',
                            marginRight: 'auto', width: '70%'}} className={"storeLogo"} src={window.URL.createObjectURL(images[0])}/>
                    </Card>
                </div>
            </Fragment>);
        }

        return (
            <div style={{width: 114, height: 114}}>
                <DropZone label={this.props.title}
                               type="image"
                          onDrop={(files, acceptedFiles, rejectedFiles) => {
                              this.setState({images: acceptedFiles.slice(0,1)});
                              this.uploadImage(acceptedFiles.slice(0,1)[0])
                          }}
                >
                    <DropZone.FileUpload />
                </DropZone>
            </div>
        );
    }

    uploadImage(image){
        console.info('entra en el upload react')
        console.log('Antes de estar en cenit',this.state.images);
        const{title, onChange}=this.props;
        const reader = new FileReader();

        reader.readAsDataURL(image);
        reader.onload = function () {
            let document = reader.result;
            console.info('va a conectarse a cenit',document)
            console.info('va a conectarse a cenit',image.name.split('.').pop().toLowerCase())
                axios.post('https://cenit.io/app/cautionem-dev/upload_image', {
                    params:{
                        ext: image.name.split('.').pop().toLowerCase(),
                        data: document,
                        name: title
                    }}, {
                    crossDomain:true,

                }).then(response => {
                    onChange(response.data);
                    console.log('id',response.data);
                })
                }
        console.log('Después de estar en cenit',this.state.images);
    }
}

export default ImageDropZone;
