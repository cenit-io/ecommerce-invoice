import React, {Fragment} from 'react';
import {Card, Banner, Link} from '@shopify/polaris';
import axios from "axios";

export default class Templates extends React.Component{
    state={
        itemList:[
            {
                id: 341,
                name: 'Document Setting 1',
                location: 'Decatur, USA',
                latestOrderUrl: 'orders/1456',
            },
            {
                id: 256,
                name: 'Document Setting 1',
                location: 'Los Angeles, USA',
                latestOrderUrl: 'orders/1457',
            },
            {
                id: 257,
                name: 'Document Setting 3',
                location: 'Los Angeles, USA',
                latestOrderUrl: 'orders/1457',
            }
        ]
    }

    toggleState = (key) => {
        return (ev) => {
            this.props.history.push(key);
        };
    };

    loadData =()=>{

    }


    render (){
        return(
            <Card sectioned title={'TEMPLATES'}>
                {/*<Icon source={}/>*/}
                <Banner icon="circleChevronRight" title={<Link onClick={this.toggleState('/invoice')}>Invoice</Link>}>
                    <p>Templates Listing</p>
                </Banner>
                <Banner icon="circleChevronRight" title={<Link>Packing Slip</Link>}>
                    <p>Templates Listing</p>
                </Banner>
            </Card>
        );
    }

}