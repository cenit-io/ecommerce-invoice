import React from 'react';
import {Page} from "@shopify/polaris";
import {Route, Switch} from 'react-router-dom';
import InvoiceTemplate from "./InvoiceTemplate";
import Configuration from "../../configurations/Configuration";
import DocSettingList from "./DocSettingList";

class InvoiceContainer extends React.Component{

    render() {
        return(
            <Page>
                <Switch>
                    {/*<Route exact path={'/invoice'} component={InvoiceTemplate}/>*/}
                    <Route exact path={'/invoice'} component={DocSettingList}/>
                    <Route path={'/invoice/configuration'} component={Configuration}/>
                </Switch>
            </Page>
        );
    }

}export default InvoiceContainer;