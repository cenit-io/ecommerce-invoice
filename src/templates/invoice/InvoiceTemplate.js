import React from 'react';
import {Card, Button, ButtonGroup} from '@shopify/polaris';


class InvoiceTemplate extends React.Component{

    toggleState = (key) => {
        return (ev) => {
            console.log(ev)
            this.props.history.push(key);
        };
    };


    render() {
        return(
            <Card>
                <Card.Section title={'INVOICE TEMPLATES'}>
                    <ButtonGroup>
                        <Button primary onClick={this.toggleState('/invoice/configuration?template=invoice1')} >INVOICE 1</Button>
                        <Button primary onClick={this.toggleState('/invoice/configuration?template=invoice2')}>INVOICE 2</Button>
                    </ButtonGroup>
                </Card.Section>
            </Card>
        );
    }

}export default InvoiceTemplate;