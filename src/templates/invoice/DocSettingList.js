import React from 'react';
import {Avatar, Button, Card, FormLayout, Modal, ResourceList, TextStyle} from "@shopify/polaris";
import SelectCombo from "../../utils/SelectCombo";
import axios from "axios";

class DocSettingList extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            selectedItems: [],
            sortValue: 'DATE_MODIFIED_DESC',
            searchValue: '',
            appliedFilters: [],
            modalActive: false,
            template_selected:'invoice_1',
            config_selected:'config_1',
            config_options: []
        };

    }


    itemsList = [
        {
            id: 341,
            name: 'Document Setting 1',
            location: 'Decatur, USA',
            latestOrderUrl: 'orders/1456',
        },
        {
            id: 256,
            name: 'Document Setting 1',
            location: 'Los Angeles, USA',
            latestOrderUrl: 'orders/1457',
        },
        {
            id: 257,
            name: 'Document Setting 3',
            location: 'Los Angeles, USA',
            latestOrderUrl: 'orders/1457',
        }
    ]

    template_options =[
        {label:'INVOICE 1',value:'invoice_1'},
        {label:'INVOICE 2',value:'invoice_2'},
    ]




     shortcutActions = [{content: 'Preview'}];



    resourceName = {
        singular: 'document',
        plural: 'documents',
    };

    // promotedBulkActions = [
    //     {
    //         content: 'Preview Document'
    //         // onAction: () => console.log('Todo: implement bulk edit')
    //     }
    // ];

    handleSearchChange = (searchValue) => {
        this.setState({searchValue});
    };
    handleFiltersChange = (appliedFilters) => {
        this.setState({appliedFilters});
    };
    handleSelectionChange = (selectedItems) => {
        console.info("items", selectedItems)
        this.setState({selectedItems});
    };

    handleSortChange=(selected)=>{
        this.setState({sortValue: selected});
        console.log(`Sort option changed to ${selected}.`);
    }

    toggleState = (key) => {
        return (ev) => {
            console.log(ev)
            this.props.history.push(key);
        };
    }

    viewModal = () => {
        this.setState(({modalActive})=>({modalActive:!modalActive}));
        }

    handleChange =(key)=>{
        return (value) => {
            const state = {};
            state[key] = value;
            this.setState(state);
        }
    }

    loadData =()=>{
        if (this.state.config_options.length ===0){
        console.log('Iniciando el metodo loadData')
        axios.get(
            'https://cenit.io/app/cautionem-dev/configurationList'
        ).then( response=>{
            console.log('a',this);
            //const selected = response.data;
            this.setState({config_options: response.data.concat(),modalActive:true})
            console.log('Metodo load data',response.data);
        }).catch(e =>{
            console.log(e);
        });
        }else{
            this.viewModal();
        }
    }

    renderItem = (item) => {
        const {id, name, location, latestOrderUrl} = item;
        return (
            <ResourceList.Item id={id} shortcutActions={this.shortcutActions} persistActions
                               onClick={this.toggleState('/invoice/configuration')}>
                <TextStyle variation="strong">{name}</TextStyle>
            </ResourceList.Item>
        );
    };

    saveInCenit =()=>{

        console.log('Solicitud post con axios');
        axios.post('https://cenit.io/app/cautionem-dev/document',this.state).then(response=>{console.log('Response', response.data);
            const myOptions= response.data;
        }).catch(e =>{console.log(e);});
    }



    render() {
        const filterControl = (
            <ResourceList.FilterControl filters={this.filters} appliedFilters={this.state.appliedFilters}
                                        onFiltersChange={this.handleFiltersChange}searchValue={this.state.searchValue}
                                        onSearchChange={this.handleSearchChange} />
        );
        console.log('Segundo',this.config_options);
        return(
            <div>
            <Card title={'DOCUMENTS'}>
                <ResourceList resourceName={this.resourceName} items={this.itemsList} renderItem={this.renderItem}
                              selectedItems={this.state.selectedItems} onSelectionChange={this.handleSelectionChange}
                              sortValue={this.state.sortValue} filterControl={filterControl} //promotedBulkActions={this.promotedBulkActions}
                              onSortChange={this.handleSortChange} alternateTool={<Button onClick={this.loadData}>New</Button>}/>
            </Card>
                <Modal title={'New Document'} open={this.state.modalActive} onClose={this.viewModal} primaryAction={{content:'Save',onAction: this.saveInCenit}}>
                    <Modal.Section>
                        <FormLayout>
                            <SelectCombo title={'Template'} options={this.template_options} onChange={this.handleChange('template_selected')}/>
                            <SelectCombo title={'Configuration'} options={this.state.config_options} onChange={this.handleChange('config_selected')}/>
                        </FormLayout>
                    </Modal.Section>
                </Modal>
            </div>
        );
    }


}export default DocSettingList