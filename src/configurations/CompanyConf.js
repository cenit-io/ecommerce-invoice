import React from 'react';
import {Card, FormLayout, TextField, TextStyle} from "@shopify/polaris";
import CheckBoxElement from "../utils/CheckBoxElement";
import ConfigContext from "../contexts/ConfigContext";


class CompanyConf  extends React.Component{
    constructor(props) {
        super(props);
        this.state={data:this.props.data}
    }


    table_elements= [
        {
            id: 'name',
             checkbox_label: 'Company Name',
        },
        {
            id: 'email',
             checkbox_label: 'Email',

        },
        {
            id: 'street1',
             checkbox_label: 'Street1',
        },
        {
            id: 'street2',
             checkbox_label: 'Street2',
        },
        {
            id: 'city',
             checkbox_label: 'City',
        },
        {
            id: 'zip',
             checkbox_label: 'Zip/Postal Code',
        },
        {
            id: 'country',
            checkbox_label: 'Country',
        },
        {
            id: 'stamp',
             checkbox_label: 'Stamp',
        },
        {
            id: 'logo',
             checkbox_label: 'Logo',
        },
    ];



    handleChange = (id) => (value) => {
        this.props.update('info_config',id,2, value)
    };

    render() {
        const table_elements = this.table_elements;
        return(
            <Card.Section title={'Company Info'}>
                <div style={{width: 450}}>
                    <div className="formContainer" >
                        <FormLayout >
                            <FormLayout.Group condensed>
                                <TextStyle> </TextStyle>
                                <TextStyle> FONT SIZE</TextStyle>
                            </FormLayout.Group>
                            <ConfigContext.Consumer>
                                {({data,update})=>table_elements.map((item)=>
                                    <FormLayout.Group condensed key={item.id}>
                                        <CheckBoxElement data={data.info_config[item.id]}  update={update}
                                                          checked={data.info_config[item.id][1]} id={item.id}
                                                          label={item.checkbox_label}  tab_name={'info_config'}/>
                                        <TextField  id={item.id} value={data.info_config[item.id][2]} onChange={this.handleChange(item.id)} disabled={!data.info_config[item.id][1]}/>
                                    </FormLayout.Group>
                                )}
                            </ConfigContext.Consumer>
                        </FormLayout>
                    </div>
                </div>
            </Card.Section>
        )
    }

}export default CompanyConf;