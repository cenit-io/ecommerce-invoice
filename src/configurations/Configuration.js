import React from 'react';
import {Card, Checkbox, FormLayout, Tabs, TextField, TextStyle} from '@shopify/polaris';
import SelectionCheckBox from "../utils/SelectionCheckBox";
import SelectCombo from "../utils/SelectCombo";
import ColorPopover from "../utils/ColorPopover";
import ImageDropZone from "../utils/ImageDropZone";
import LabelsConfig from "./LabelsConfig";
import InfoConfig from "./InfoConfig";
import axios from "axios";
import ConfigContext from "../contexts/ConfigContext";

class Configuration extends React.Component{


    constructor(props) {
        super(props);

        const params = new URLSearchParams(props.location.search);

        this.state = {
            selected: 0,
            view:false,
            data:{
                config_name: 'New Configuration',
                page_size: 'a4',
                date_format: 'today',
                font: '1',
                regular_color: '#000000',
                contrast_highlight_color: '#F07878',
                highlight_color: '#552720',
                background_image: '',
                stamp_image:'',
                background_color: '#FFFFFF',
                template_selected:'invoice_1',
                createChecked: false,
                labels_config:{
                    invoice:[true, true,'Invoice','Factura'],
                    client:[true, true,'Client','Cliente'],
                    provider:[true, true,'Provider','Proveedor'],
                    total:[true, true,'Total','Total'],
                    subtotal:[true, true,'Sub Total','Subtotal'],
                    tax:[true, true,'Tax','Impuestos'],
                    balanceDue:[true, true,'Balance Due','Saldo Deudor'],
                    invoiceDate:[true, true,'Invoice Date','Fecha de la Factura'],
                    dueDate:[true, true,'Due Date','Fecha de vencimiento'],
                    item:'',
                },
                info_config:{
                    name: ['Company Name',true,'20'],
                    email: ['Email',true,'20'],
                    street1: ['Street1',true,'20'],
                    street2: ['Street2',true,'20'],
                    city: ['City',true,'20'],
                    zip: ['Zip/Postal Code',true,'20'],
                    stamp: ['Stamp',true,'20'],
                    logo: ['Logo',true,'20'],
                    country:['Country',true,'20'],
                    no: ['#',true,'20'],
                    item_table: ['Item',true,'20'],
                    description: ['Description',true,'20'],
                    quantity: ['Qty',true,'20'],
                    sku: ['Sku',true,'20'],
                    price: ['Price',true,'20'],
                    tax: ['Tax Amount',true,'20'],
                    amount: ['Amount',true,'20'],
                }
            }
        };
    }

    dateFormatOptions =[
        {label: 'YYYYMMDD', value: 'uno'},
        {label: 'YYYYMMDD HH:MI:SS', value: 'dos'},
        {label: 'YYYY-MM-DD', value: 'tres'},
        {label: 'YYYY-MM-DD HH:MI:SS', value: 'cuatro'}
    ]

    font_options=[
        {label:'FONT 1',value:'1'},
        {label:'FONT 2',value:'2'},
        {label:'FONT 3',value:'3'}
    ]

    template_options=[
        {label:'INVOICE 1',value:'invoice_1'},
        {label:'INVOICE 2',value:'invoice_2'},
    ]

    paper_options=[
        {label:'A4',value:'a4'},
        {label:'LETTER 2',value:'letter'},
    ]



    next =()=>{
        this.setState({selected: this.state.selected + 1})
        if (this.state.selected === 3)
            this.setState({view:true})
    }

    back=()=>{
        this.setState({selected: this.state.selected - 1})
    }

    handleChange(key){
        return (value) => {
            this.setState(prevstate=>{
                prevstate.data[key]=value;
                return prevstate
            });
        }
    }
    handlePreview=()=> {
        const {config_name, page_size, date_format, font, regular_color, contrast_highlight_color, highlight_color, background_image,template_selected,createChecked} = this.state.data;
        console.info('template', template_selected)
        const params = {
            config_name: config_name,
            page_size: page_size,
            date_format: date_format,
            font: font,
            regular_color: regular_color,
            contrast_highlight_color: contrast_highlight_color,
            highlight_color: highlight_color,
            background_image: background_image,
            template_selected:template_selected,
            createChecked:createChecked
        };
        const query = Object.keys(params).map((key) => {
            return `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`
        }).join('&');
        window.open(`https://cenit.io/app/cautionem-dev/pdf_test?${query}`)
    }

    saveInCenit =()=>{
        console.log(this.state.data)
        //solicitud post con axios.
        axios.post('https://cenit.io/app/cautionem-dev/configuration',this.state.data).then(response=>{console.log(response);}).catch(e =>{console.log(e);});
    }

    update=(tab,id,index,value)=>{
        const labels = this.state.data;
        labels[tab][id][index]=value;
        this.setState({data: labels})
    }



    render() {

        const {config_name, page_size, regular_color, highlight_color, contrast_highlight_color, background_color,font,template_selected,date_format,createChecked} = this.state.data;
        const propertiesView =(
            <Card  primaryFooterAction={{ content: 'Next',onAction: this.next}}>
                <Card.Section title={'GENERAL'}>
                    <FormLayout.Group>
                        <SelectCombo title={'Template'} value={template_selected} options={this.template_options}  onChange={this.handleChange('template_selected')}/>
                        <TextField  label="Name" value={config_name} onChange={this.handleChange('config_name')}/>
                        <FormLayout condensed>
                            <TextStyle>Create with template</TextStyle>
                            <Checkbox checked={createChecked} onChange={this.handleChange('createChecked')}/>
                        </FormLayout>
                    </FormLayout.Group>
                </Card.Section>
                <Card.Section title={'FONT CONFIGURATION'}>
                    <FormLayout.Group>
                        <SelectCombo title={'Paper size'} value={page_size} options={this.paper_options}  onChange={this.handleChange('page_size')}/>
                        <SelectCombo title={'Font'} value={font} options={this.font_options}  onChange={this.handleChange('font')}/>
                        <SelectCombo title={'Date format'} value={date_format} options={this.dateFormatOptions} onChange={this.handleChange('date_format')}   />
                    </FormLayout.Group>
                    <FormLayout.Group>
                        <ColorPopover text_label='Regular' onChange={this.handleChange('regular_color')} color={regular_color}/>
                        <ColorPopover text_label='Highlight' onChange={this.handleChange('highlight_color')} color={highlight_color}/>
                        <ColorPopover text_label='Contrast highlight' onChange={this.handleChange('contrast_highlight_color')}color={contrast_highlight_color}/>
                    </FormLayout.Group>
                </Card.Section>
                <Card.Section title={'BACKGROUND'}>
                    <FormLayout.Group>
                        <ColorPopover text_label='Color'  onChange={this.handleChange('regular_color')} color={background_color}/>
                        <ImageDropZone title='Image' onChange={this.handleChange('background_image')} />
                    </FormLayout.Group>
                </Card.Section>
            </Card>
        );


        const tabs = [
            {
                id: 'propeties',
                content: 'Properties',
                accessibilityLabel: propertiesView,
                panelID: 'propertiesId'
            },
            {
                id: 'labels',
                content: 'Labels',
                accessibilityLabel: <LabelsConfig data={this.state.data.labels_config} back={this.back} next={this.next}/>,
                panelID: 'labelsId'
            },
            {
                id: 'info',
                content: 'Info',
                accessibilityLabel: <InfoConfig data={this.state.data.info_config} back={this.back} update={this.update} save={this.saveInCenit}/>,
                panelID: 'infoId'
            },
        ];

        return(
            <ConfigContext.Provider value={{data: this.state.data, update: this.update, tab_name: ''}}>
                <Card>
                    <Tabs selected={this.state.selected} tabs={tabs}>
                        <Card.Section title={tabs[this.state.selected].accessibilityLabel}>
                        </Card.Section>
                    </Tabs>
                </Card>
            </ConfigContext.Provider>
        )
    }

}export default Configuration;