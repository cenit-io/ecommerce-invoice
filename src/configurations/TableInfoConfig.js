import React from 'react';
import {Card, FormLayout, TextField, TextStyle} from "@shopify/polaris";
import CheckBoxElement from "../utils/CheckBoxElement";
import ConfigContext from "../contexts/ConfigContext";


class TableInfoConfig  extends React.Component{
    constructor(props) {
        super(props);
        this.state={data:this.props.data}
    }

    table_elements= [
        {
            id: 'no',
            checkbox_label: 'No.',
            // width: '',
            // label: '#',
            // selected: true
        },
        {
            id: 'item_table',
            checkbox_label: 'Item',
            // width: '',
            // label: 'Item',
            // selected: true
        },
        {
            id: 'description',
            checkbox_label: 'Description',
            // width: '',
            // label: 'Description',
            // selected: true
        },
        {
            id: 'quantity',
            checkbox_label: 'Quantity',
            // width: '',
            // label: 'Qty',
            // selected: true
        },
        {
            id: 'sku',
            checkbox_label: 'Sku',
            // width: '',
            // label: 'Sku',
            // selected: true
        },
        {
            id: 'price',
            checkbox_label: 'Price per item',
            // width: '',
            // label: 'Price',
            // selected: true
        },
        {
            id: 'tax',
            checkbox_label: 'Tax Amount',
            // width: '',
            // label: 'Tax Amount',
            // selected: true
        },
        {
            id: 'amount',
            checkbox_label: 'Amount',
            // width: '',
            // label: 'Amount',
            // selected: true
        },
    ];

    // changeShow=(id, active)=>{
    //     const elements = this.state.elements;
    //     elements[id] = active;
    //     this.setState({elements})
    // }

    handleWidthChange = (id) => (value) => {
        this.props.update('info_config',id,2, value)
    };

    handleLabelChange = (id) => (value) => {
        this.props.update('info_config',id,0, value)
    };

    render() {
        const table_elements = this.table_elements;
        return(
            <Card >
                <Card.Section title={'Table items'}>
                    <div style={{width: 450}}>
                        <div className="formContainer" >
                            <FormLayout >
                                <FormLayout.Group condensed>
                                    <TextStyle>      </TextStyle>
                                    <TextStyle> LABEL</TextStyle>
                                    <TextStyle> WIDTH</TextStyle>
                                </FormLayout.Group>
                                <ConfigContext.Consumer>
                                {({data,update})=>table_elements.map((item)=>
                                    <FormLayout.Group condensed key={item.id}>
                                        <CheckBoxElement  update={update} data={data.info_config[item.id]}
                                                         id={item.id} label={item.checkbox_label} checked={data.info_config[item.id][1]}
                                                         tab_name={'info_config'}  />
                                        <TextField value={data.info_config[item.id][0]} onChange={this.handleLabelChange(item.id)} disabled={!data.info_config[item.id][1]}/>
                                        <TextField value={data.info_config[item.id][2]} onChange={this.handleWidthChange(item.id)} disabled={!data.info_config[item.id][1]}/>
                                    </FormLayout.Group>
                                )}
                                </ConfigContext.Consumer>
                            </FormLayout>
                        </div>
                    </div>
                </Card.Section>
            </Card>
        );
    }


}export default TableInfoConfig;