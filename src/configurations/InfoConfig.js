import React from 'react';
import LabelsConfig from "./LabelsConfig";
import {Card, Tabs} from "@shopify/polaris";
import TableInfoConfig from "./TableInfoConfig";
import CompanyConf from "./CompanyConf";
import ConfigContext from "../contexts/ConfigContext";

class InfoConfig extends React.Component{

    constructor(props) {
        super(props);
        this.state ={
            data:this.props.data,
            selected: 0
        }
    }



    tabChange =(selectedTabIndex)=>{
        this.setState({selected: selectedTabIndex})
    }

    render() {
        const tabs = [
            {
                id: 'company',
                content: 'Comany Info',
                accessibilityLabel: <CompanyConf update={this.props.update}/>,
                panelID: 'propertiesId'
            },
            {
                id: 'table',
                content: 'Table Items',
                accessibilityLabel:<TableInfoConfig  update={this.props.update}/>,
                panelID: 'labelsId'
            },
        ];


        return(
            <Card actions={{content:'Back',onAction: this.props.back}} primaryFooterAction={{ content: 'Save',onAction: this.props.save}}>
                    <Tabs selected={this.state.selected} tabs={tabs} onSelect={this.tabChange}>
                        <Card.Section title={tabs[this.state.selected].accessibilityLabel}>
                        </Card.Section>
                    </Tabs>
            </Card>
        );

    }







}export default InfoConfig;