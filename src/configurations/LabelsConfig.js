import React from 'react';
import {Card, FormLayout, Stack, TextField, TextStyle} from '@shopify/polaris';
import Labels from "../utils/Labels";
import ConfigContext from "../contexts/ConfigContext";


class LabelsConfig extends React.Component{
    constructor(props) {
        super(props);
        this.state={data:this.props.data}
    }

    elements=[
        {
            id:'invoice',
        },
        {
            id: 'client',
        },
        {
            id: 'provider',
        },
        {
            id: 'total',
        },
        {
            id: 'subtotal',
        },
        {
            id: 'tax',
        },
        {
            id:'balanceDue',
        },
        {
            id: 'invoiceDate',
        },
        {
            id: 'dueDate',
        },
    ]

    render() {
        const elements = this.elements;
        return(

            <Card primaryFooterAction={{ content: 'Next',onAction: this.props.next}} actions={{content:'Back',onAction: this.props.back}}>
                <Card.Section title={'LABELS & SUBLABELS'}>
                    <Stack spacing="tight">
                        <FormLayout>
                            <Stack wrap={false} alignment="leading">
                                <div style={{transform:'translateX(50px)'}}><TextStyle> label</TextStyle></div>
                                <div style={{transform:'translateX(250px)'}}><TextStyle> sublabel</TextStyle></div>
                            </Stack>
                            <Stack wrap={false} alignment="leading">
                                <ConfigContext.Consumer>
                                    {({data,update})=>elements.map((item)=>
                                        <Labels id={item.id} key={item.id} data={data.labels_config[item.id]}
                                                tab_name={'labels_config'} update={update}/>
                                    )}
                                </ConfigContext.Consumer>
                            </Stack>
                        </FormLayout>
                    </Stack>
                </Card.Section>
            </Card>

        );
    }

}export default LabelsConfig;